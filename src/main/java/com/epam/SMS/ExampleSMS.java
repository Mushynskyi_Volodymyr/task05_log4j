package com.epam.SMS;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "AC85a92ef27735a6ea9052a054cba5f8a4";
    public static final String AUTH_TOKEN = "5855e21d9f918dc7ec4e0cc6d9d456d2";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380676736881"),
                new PhoneNumber("+12054420909"), str) .create();
    }
}
